# Entity Relationship Diagram

Please note that this is a work in progress. Plenty more entities and relationships to include. 

If you are not familiar with ERDs, [here](https://www.lucidchart.com/pages/ER-diagram-symbols-and-meaning) is a helpful guide.

<div style="width: 640px; height: 480px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://app.lucidchart.com/documents/embeddedchart/b13675bb-1833-4e98-b5b5-6661c1f6aa59" id="KCTvniKkOLfQ"></iframe></div>
